require('./utils/handlebars-helper')
const path = require('path')
const cors = require('cors')
const express = require('express')
const router = require('./routes/route')
const bodyParser = require('body-parser')
const app = express()
const PORT = process.env.PORT || 3000

// Enable CORS
app.use(cors())

// set view dan view engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// set body-parser
app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: false
    })
);

// set public folder as assets
app.use('/assets', express.static(__dirname + '/public'));

// set routes
app.use('/', router);

// buat server
app.listen(PORT, () => {
    console.log(`Server running at port ${PORT}`);
});