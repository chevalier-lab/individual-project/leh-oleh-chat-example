const fetch = require('node-fetch')

// direct index
exports.index = (req, res) => {
    res.render('index', {
        title: 'Leh-oleh chat'
    })
}

// detail chat
exports.chat = async(req, res) => {
    // get raw data
    let { user_token } = req.body

    try {
        // check type of user
        let fetchUser = await fetch('http://213.190.4.40:5050/api/chat/room/check', {
            headers: { 'Authorization': user_token }
        })
        let responseUser = await fetchUser.json()

        // if token is cs
        if (responseUser.data == 2 || responseUser.data == 4) {
            return res.redirect(`/list-chat?token=${user_token}`)
        }

        // fetch data room (search data room by id_m_users_1)
        let fetchRoom = await fetch('http://213.190.4.40:5050/api/chat/room/guest', {
            headers: { 'Authorization': user_token }
        })
        let responseRoom = await fetchRoom.json()

        // init data user
        let user = {
            token: user_token,
            full_name: responseRoom.data.full_name_user_1,
            friend_full_name: responseRoom.data.full_name_user_2
        }

        // init data room
        let room = { 
            id: responseRoom.data.id,
            token: responseRoom.data.chat_token 
        }

        res.render('chat', {
            user,
            room,
            type: 'customer',
            title: 'Room chat'
        })
    } catch (err) {
        console.log(err)
    }

}

// list chat (if user is cs)
exports.listChat = async(req, res) => {
    // get query params
    let token = req.query.token

    try {
        // fetch data room (search data room by id_m_users_1)
        let fetchRoom = await fetch('http://213.190.4.40:5050/api/chat/room/cs', {
            headers: { 'Authorization': token }
        })
        let responseRoom = await fetchRoom.json()

        res.render('list-chat', { 
            title: 'list chat',
            csToken: token,
            data: responseRoom.data
        })
    } catch (err) {
        console.log(err);
    }
}

// detail chat cs
exports.chatCs = async(req, res) => {
    // get data
    let { room_id, cs_token } = req.body
    
    try {
        let fetchRoom = await fetch(`http://213.190.4.40:5050/api/chat/room/cs/${room_id}`, {
            headers: { 'Authorization': cs_token }
        })
        let responseRoom = await fetchRoom.json()

        // init data user
        let user = {
            token: cs_token,
            full_name: responseRoom.data.full_name_user_2,
            friend_full_name: responseRoom.data.full_name_user_1
        }

        // init data room
        let room = { 
            id: responseRoom.data.id,
            token: responseRoom.data.chat_token 
        }

        res.render('chat', {
            user,
            room,
            type: 'cs',
            title: 'Room chat'
        })
    } catch (err) {
        console.log(err);
    }
}