const friendName = document.getElementById('friend')
const friendStatus = document.getElementById('status')
const chatForm = document.getElementById('chat-form')
const btnDeactive = document.getElementById('btn-deactive')
const chatMessages = document.querySelector('.chat-messages')
const socket = io("http://213.190.4.40:2020", { path: '/leh-oleh/chat' })

// set heartbeat timeout
socket.heartbeatTimeout = 20000

// get utils data
const dataUser = {
    token: document.getElementById('user-token').value,
    userName: document.getElementById('user-fullname').value,
    friendName: document.getElementById('friend-fullname').value
}
const dataRoom = {
    id: document.getElementById('room-id').value,
    token: document.getElementById('room-token').value
}

// set friend name
friendName.innerText = dataUser.friendName

// join room by token
socket.emit('token', { userId: dataUser.userName, token: dataRoom.token })

// get user info
socket.on('user-info', ({ token, friend }) => {
    // get friend data
    const user = friend.filter(user => user.userId !== dataUser.userName)

    // set user-status
    if (user.length !== 0) {
        friendStatus.innerText = 'online'
    } else {
        friendStatus.innerText = 'offline'
    }
    
    console.log(user);
})

// get message from server
socket.on('message', data => {
    console.log(data)

    // check user
    if (data.username == dataUser.userName) {
        outputMessage(data)
    } else {
        ouputMessageFriend(data)
    }

    // scroll down automatically
    chatMessages.scrollTop = chatMessages.scrollHeight
})

// get status message from server 
socket.on('status', ({ status, user }) => { 
    if (user.userId !== dataUser.fullName && status == 'offline') {
        friendStatus.innerText = 'offline'
    }

    // log status
    console.log(`${user.userId} is ${status}`);
})

// message submit
chatForm.addEventListener('submit', e => {
    e.preventDefault()

    // get message text
    const message = e.target.elements.message.value

    // encrypt
    const encryptMessage = CryptoJS.AES.encrypt(message, 'secret-chat').toString()

    // send message to server
    socket.emit('chat-message', message)

    // post data (id_chat masih dummy)
    fetch('http://213.190.4.40:5050/api/chat/room/detail', {
        method: 'POST',
        body: JSON.stringify({ 
            id: dataRoom.id,
            content: encryptMessage
        }),
        headers: {
            'Authorization': dataUser.token,
            'Content-Type': 'application/json'
        }
    })
        .then(res => res.json())
        .then(res => console.log(res))
        .catch(err => console.log(err))

    // clear input
    e.target.elements.message.value = ''
    e.target.elements.message.focus()
})

// deactive chat room
if (btnDeactive != null) {
    btnDeactive.addEventListener('click', () => {
        fetch('http://213.190.4.40:5050/api/chat/room', {
            method: 'PUT',
            body: JSON.stringify({ id: dataRoom.id }),
            headers: {
                'Authorization': dataUser.token,
                'Content-Type': 'application/json'
            }
        })
            .then(res => res.json())
            .then(res => {
                console.log(res)
                window.location.href = `/list-chat?token=${dataUser.token}`
            })
            .catch(err => console.log(err))
    })
}
