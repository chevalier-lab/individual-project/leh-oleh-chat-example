// decrypt
function decrypt(msg) {
    let bytes = CryptoJS.AES.decrypt(msg, 'secret-chat')
    return bytes.toString(CryptoJS.enc.Utf8)
}

// output message to DOM
function outputMessage(data) {
    const div = document.createElement('div')
    div.classList.add('message')
    div.innerHTML = `
        <p class="meta" style="text-align: right"><span style="font-size: 10pt">${data.time}</span> you</p>
        <p class="text" style="text-align: right">
            ${data.text}
        </p>
    `

    // append div 
    document.querySelector('.chat-messages').appendChild(div)
}

function ouputMessageFriend(data) {
    const div = document.createElement('div')
    div.classList.add('message')
    div.innerHTML = `
        <p class="meta">${data.username} <span style="font-size: 10pt">${data.time}</span></p>
        <p class="text">
            ${data.text}
        </p>
    `

    // append div 
    document.querySelector('.chat-messages').appendChild(div)
}

// output message from db
function outputMessageDb(data) {
    let time = data.created_at.split(' ')[1]
    let timeSplit = time.split(':')
    let hour = timeSplit[0].concat('.', timeSplit[1])
    const div = document.createElement('div')
    div.classList.add('message')
    div.innerHTML = `
        <p class="meta" style="text-align: right"><span style="font-size: 10pt">${hour}</span> you</p>
        <p class="text" style="text-align: right">
            ${decrypt(data.content)}
        </p>
    `

    // append div 
    document.querySelector('.chat-messages').appendChild(div)
}

function ouputMessageFriendDb(data) {
    let time = data.created_at.split(' ')[1]
    let timeSplit = time.split(':')
    let hour = timeSplit[0].concat('.', timeSplit[1])
    const div = document.createElement('div')
    div.classList.add('message')
    div.innerHTML = `
        <p class="meta">${dataUser.friendName} <span style="font-size: 10pt">${hour}</span></p>
        <p class="text">
            ${decrypt(data.content)}
        </p>
    `

    // append div 
    document.querySelector('.chat-messages').appendChild(div)
}