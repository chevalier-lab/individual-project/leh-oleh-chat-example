const express = require('express')
const router = express.Router()
const { index, chat, listChat, chatCs } = require('../controllers/chat')

router.route('/').get(index)
router.route('/chat').post(chat)
router.route('/chat/cs').post(chatCs)
router.route('/list-chat').get(listChat)

module.exports = router